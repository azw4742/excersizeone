package com.example.andrew.exerciseone;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import java.text.DecimalFormat;

public class exerciseOne extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_one);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercise_one, menu);
        return true;
    }

    private EditText loanAmount;
    private EditText intrestRate;
    private TextView fiveYears;
    private TextView tenYears;
    private TextView fifteenYears;
    private TextView twentyYears;
    private TextView twentyFiveYears;
    private TextView thirtyYears;

    public void onClick(View v)
    {
        loanAmount = (EditText) findViewById(R.id.loanAmount);
        intrestRate = (EditText) findViewById(R.id.intrestRate);
        fiveYears = (TextView) findViewById(R.id.fiveYears);
        tenYears = (TextView) findViewById(R.id.tenYears);
        twentyYears = (TextView) findViewById(R.id.twentyYears);
        twentyFiveYears = (TextView) findViewById(R.id.twentyFiveYears);
        fifteenYears = (TextView) findViewById(R.id.fifteenYears);
        thirtyYears = (TextView) findViewById(R.id.thirtyYears);
        Double loanVal = Double.parseDouble(loanAmount.getText().toString());
        Double intrestRateVal = Double.parseDouble(intrestRate.getText().toString());
        Integer years = 5;
        String output = calculate(loanVal, intrestRateVal,years) + "";
        fiveYears.setText(output);
        years = 10;
        output = calculate(loanVal, intrestRateVal,years) + "";
        tenYears.setText(output);
        years = 15;
        output = calculate(loanVal, intrestRateVal,years) + "";
        fifteenYears.setText(output);
        years = 20;
        output = calculate(loanVal, intrestRateVal,years) + "";
        twentyYears.setText(output);
        years = 25;
        output = calculate(loanVal, intrestRateVal,years) + "";
        twentyFiveYears.setText(output);
        years = 30;
        output = calculate(loanVal, intrestRateVal,years) + "";
        thirtyYears.setText(output);
    }

    public String calculate(Double loanAmount, Double intrestRate,Integer years)
    {
        Double value = (loanAmount * intrestRate * (1 + intrestRate)*years) / ((1 + intrestRate)* (years - 1));
        DecimalFormat df = new DecimalFormat("#.00");
        String outputValue = df.format(value);
        return outputValue;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
